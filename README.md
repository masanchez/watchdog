Watchdog Plugin
------------

Watchdog is a logging plugin for CakePHP 3.x. The behavior tracks changes on models.

Requirements
------------

* CakePHP 3.x+
* PHP 5.5.x+

Documentation
-------------

For documentation, as well as tutorials, see the [Docs](Docs/Home.md) directory of this repository.

License
-------

Licensed under the [MIT](http://www.opensource.org/licenses/mit-license.php) License. Redistributions of the source code included in this repository must retain the copyright notice found in each file.