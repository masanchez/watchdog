Home
====

IMPORANT: Current status is **BETA**. We are still working on improving the unit test coverage and docs
Plugin is usable now if you want to download and take a look. PRs and Issues welcome!

The **Watchdog** Plugin allow users to logging actions.

Requirements
------------

* CakePHP 3.x+
* PHP 5.4.x+

Documentation
-------------

* [Overview](Documentation/Overview.md)
* [Installation](Documentation/Installation.md)
* [Extending the Plugin](Documentation/Extending-the-Plugin.md)
