Installation
============

Composer
------

```
composer require masanchez/watchdog
```

Creating Required Tables
------------------------
If you want to use the Users tables to store your users and social accounts:

```
bin/cake migrations migrate -p Masanchez/Watchdog
```


Load the Plugin
-----------

Ensure the Users Plugin is loaded in your config/bootstrap.php file

```
Plugin::load('Masanchez/Watchdog', ['routes' => true, 'bootstrap' => true]);
```