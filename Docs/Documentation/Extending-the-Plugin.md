Extending the Plugin
====================

Extending the Model (Table/Entity)
-------------------

Create a new Table and Entity in your app, matching the table you want to use for storing the
watchdogs data. Check the initial watchdogs migration to know the default columns expected in the table.
If your column names doesn't match the columns in your current table, you could use the Entity to
match the colums using accessors & mutators as described here http://book.cakephp.org/3.0/en/orm/entities.html#accessors-mutators

Example: we are going to use a custom table in our application ```my_watchdogs```
* Create a new Table under src/Model/Table/MyWatchdogsTable.php

```php
namespace App\Model\Table;

use Masanchez\Watchdog\Model\Table\WatchdogsTable;

/**
 * Users Model
 */
class MyWatchdogsTable extends WatchdogsTable
{
}
```

* Create a new Entity under src/Model/Entity/MyWatchdog.php

```php
namespace App\Model\Entity;

use Masanchez\Watchdog\Model\Entity\Watchdog;

class MyWatchdog extends Watchdog
{
}
```

Extending the Controller
-------------------

You want to use one of your controllers to handle all the watchdog features in your app, and keep the
index/view/etc actions from Watchdog Plugin,

```php
<?php
namespace App\Controller;

use App\Controller\AppController;
use Masanchez\Watchdog\Controller\Traits\SimpleCrudTrait;

class MyWatchdogsController extends AppController
{
    use SimpleCrudTrait;

//add your new actions, override, etc here
}
```

Note you'll need to **copy the Plugin templates** you need into your project src/Template/MyWatchdogs/[action].ctp

```
Updating the Templates
-------------------

Use the standard CakePHP conventions to override Plugin views using your application views
http://book.cakephp.org/3.0/en/plugins.html#overriding-plugin-templates-from-inside-your-application