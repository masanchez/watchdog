<div class="watchdogs index large-9 medium-8 columns content">
    <h3><?= __d('Watchdog', 'Watchdogs') ?></h3>
    <table cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('uid') ?></th>
                <th><?= $this->Paginator->sort('type') ?></th>
                <th><?= $this->Paginator->sort('foreign_key') ?></th>
                <th><?= $this->Paginator->sort('message') ?></th>
                <th><?= $this->Paginator->sort('hostname') ?></th>
                <th><?= $this->Paginator->sort('timestamp') ?></th>
                <th class="actions"><?= __d('Watchdog', 'Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach (${$tableAlias} as $watchdog): ?>
            <tr>
                <td><?= $this->Number->format($watchdog->id) ?></td>
                <td><?= $this->Number->format($watchdog->uid) ?></td>
                <td><?= h($watchdog->type) ?></td>
                <td><?= $this->Number->format($watchdog->foreign_key) ?></td>
                <td><?= strtok(__d('Watchdog', $watchdog->message, json_decode($watchdog->variables)), "\n") ?></td>
                <td><?= h($watchdog->hostname) ?></td>
                <td><?= $this->Time->format($watchdog->timestamp); ?></td>
                <td class="actions">
                    <?= $this->Html->link(__d('Watchdog', 'View'), ['action' => 'view', $watchdog->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __d('Watchdog', 'previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__d('Watchdog', 'next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
