<?php $watchdog = ${$tableAlias}; ?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __d('Watchdog', 'Actions') ?></li>
        <li><?= $this->Html->link(__d('Watchdog', 'List Watchdogs'), ['action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="watchdogs view large-9 medium-8 columns content">
    <h3><?= h($watchdog->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __d('Watchdog', 'Type') ?></th>
            <td><?= h($watchdog->type) ?></td>
        </tr>
        <tr>
            <th><?= __d('Watchdog', 'Hostname') ?></th>
            <td><?= h($watchdog->hostname) ?></td>
        </tr>
        <tr>
            <th><?= __d('Watchdog', 'Id') ?></th>
            <td><?= $this->Number->format($watchdog->id) ?></td>
        </tr>
        <tr>
            <th><?= __d('Watchdog', 'Uid') ?></th>
            <td><?= $this->Number->format($watchdog->uid) ?></td>
        </tr>
        <tr>
            <th><?= __d('Watchdog', 'Foreign Key') ?></th>
            <td><?= $this->Number->format($watchdog->foreign_key) ?></td>
        </tr>
        <tr>
            <th><?= __d('Watchdog', 'Severity') ?></th>
            <td><?= $this->Number->format($watchdog->severity) ?></td>
        </tr>
        <tr>
            <th><?= __d('Watchdog', 'Timestamp') ?></th>
            <td><?= $this->Time->format($watchdog->timestamp) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __d('Watchdog', 'Message') ?></h4>
        <?= $this->Text->autoParagraph(__d('Watchdog', $watchdog->message, json_decode($watchdog->variables))); ?>
    </div>
    <div class="row">
        <h4><?= __d('Watchdog', 'Link') ?></h4>
        <?= $this->Text->autoParagraph(h($watchdog->link)); ?>
    </div>
    <div class="row">
        <h4><?= __d('Watchdog', 'Location') ?></h4>
        <?= $this->Text->autoParagraph(h($watchdog->location)); ?>
    </div>
    <div class="row">
        <h4><?= __d('Watchdog', 'Referer') ?></h4>
        <?= $this->Text->autoParagraph(h($watchdog->referer)); ?>
    </div>
</div>
