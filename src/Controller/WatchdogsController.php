<?php
namespace Masanchez\Watchdog\Controller;

use Masanchez\Watchdog\Controller\AppController;
use Masanchez\Watchdog\Controller\Traits\SimpleCrudTrait;

/**
 * Watchdogs Controller
 *
 * @property \Masanchez\Watchdog\Model\Table\WatchdogsTable $Watchdogs
 */
class WatchdogsController extends AppController
{
    use SimpleCrudTrait;
    
    /**
     * Initialize and set defaults for model class
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
    }
}
