<?php
/**
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

namespace Masanchez\Watchdog\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * RememberMe Component.
 *
 * Saves a cookie to keep the user logged into the application even when the session expires
 *
 * @link http://book.cakephp.org/3.0/en/controllers/components/cookie.html
 */
class WatchdogComponent extends Component
{
    /**
     * Initialize config data and properties.
     *
     * @param array $config The config data.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    /**
     * Sets cookie configuration options
     *
     * @return void
     */
    public function setLog($type, $foreign_key = null, $message, $variables, $severity, $link = null)
    {
        // Get User
        $user = Router::getRequest()->session()->read('Auth.User');
        
        // Save Log into Watchdog
        $dataLog = [
            'uid' => (!empty($user)) ? $user['id'] : 0,
            'type' => $type,
            'foreign_key' => $foreign_key,
            'message' => $message,
            'variables' => json_encode($variables),
            'severity' => $severity,
            'link' => $link,
            'location' => Router::url(Router::getRequest()->url, true),
            'referer' => Router::getRequest()->env('HTTP_REFERER'),
            'hostname' => Router::getRequest()->clientIp(),
            'timestamp' => time(),
        ];
        $Watchdogs = TableRegistry::get('Watchdogs');
        $watchdog = $Watchdogs->newEntity();
        $watchdog = $Watchdogs->patchEntity($watchdog, $dataLog);
        return $Watchdogs->save($watchdog);
    }

}
