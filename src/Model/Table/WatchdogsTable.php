<?php
namespace Masanchez\Watchdog\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Masanchez\Watchdog\Model\Entity\Watchdog;

/**
 * Watchdogs Model
 *
 */
class WatchdogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('watchdogs');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users', [
            'foreignKey' => 'uid'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('uid', 'valid', ['rule' => 'numeric'])
            ->requirePresence('uid', 'create')
            ->notEmpty('uid');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->add('foreign_key', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('foreign_key');

        $validator
            ->requirePresence('message', 'create')
            ->notEmpty('message');

        $validator
            ->requirePresence('variables', 'create')
            ->notEmpty('variables');

        $validator
            ->add('severity', 'valid', ['rule' => 'numeric'])
            ->requirePresence('severity', 'create')
            ->notEmpty('severity');

        $validator
            ->allowEmpty('link');

        $validator
            ->requirePresence('location', 'create')
            ->notEmpty('location');

        $validator
            ->allowEmpty('referer');

        $validator
            ->requirePresence('hostname', 'create')
            ->notEmpty('hostname');

        $validator
            ->add('timestamp', 'valid', ['rule' => 'numeric'])
            ->requirePresence('timestamp', 'create')
            ->notEmpty('timestamp');

        return $validator;
    }
    
    public function getTypes(){
        $types = $this->find('list', ['keyField' => 'type', 'valueField' => 'type'])->select(['type'])->distinct(['type']);
        return $types;
    }
}
