<?php
namespace Masanchez\Watchdog\Model\Behavior;

use Cake\Core\Configure;
use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Utility\Hash;
use Cake\Utility\Inflector;
use Cake\Routing\Router;
/**
 * WhoDidIt behavior
 *
 */
class WatchableBehavior extends Behavior
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'blacklist' => ['id', 'created', 'modified'],
        'whitelist' => []
    ];
    
    /**
     * Holder for table.
     *
     * @var \Cake\ORM\Table
     */
    protected $Table;

    /**
     * Constructor
     *
     * @param \Cake\ORM\Table $table Table who requested the behavior.
     * @param array $config Options.
     */
    public function __construct(Table $table, array $config = [])
    {
        parent::__construct($table, $config);

        $this->Table = $table;
    }

    /**
     * Initialize
     *
     * Initialize callback for Behaviors.
     *
     * @param array $config Options.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
    }


    /**
     * BeforeSave callback
     *
     * @param \Cake\Event\Event $event Event.
     * @param \Cake\ORM\Entity $entity The Entity to save on.
     * @param array $options Options.
     * @return void
     */
    public function afterSave(Event $event, EntityInterface $entity, $options)
    {
        $config = $this->_config;
        
        if (empty($config['whitelist'])) {
            $config['whitelist'] = $this->_table->schema()->columns();
            $config['whitelist'] = array_merge(
                $config['whitelist'],
                $this->getAssociationProperties(array_keys($options['associated']))
            );
        }
        
        $config['whitelist'] = array_diff($config['whitelist'], $config['blacklist']);
        
        // Get translations
        $translations = $this->getTranslatedFields($entity);
        
        $updated = $entity->extract($config['whitelist'], true);
        
        if (!$updated) {
            return;
        }
        
        $original = $entity->extractOriginal(array_keys($updated));
        $associated = [];
        
        // Clean associated models from updated and original and saved in associations
        $properties = $this->getAssociationProperties(array_keys($options['associated']));
        foreach ($properties as $property) {
            if (!empty($updated[$property])) {
                $associated[$property]['updated'] = $updated[$property];
                $associated[$property]['original'] = $original[$property];
            }
            unset($updated[$property], $original[$property]);
        }
        
        if (!$updated) {
            return;
        }
        
        // Get main model alias
        $modelName = __d('Watchdog', Inflector::singularize(Inflector::humanize($entity->source())));
        // Get User
        $user = Router::getRequest()->session()->read('Auth.User');
        
        // Automagic message generator
        $fieldCounter = 0;
        if ($entity->isNew()) {
            $message = (!empty($user)) ? "A new registry with id \"{".$fieldCounter++."}\" of table \"{".$fieldCounter++."}\" has been created by {".$fieldCounter++."}." : "A new registry with id {".$fieldCounter++."} of table \"{".$fieldCounter++."}\" has been created.";
        }else{
            $message = (!empty($user)) ? "The registry with id \"{".$fieldCounter++."}\" of table \"{".$fieldCounter++."}\" has been updated by {".$fieldCounter++."}." : "The registry with id {".$fieldCounter++."} of table \"{".$fieldCounter++."}\" has been updated.";
        }
        $variables = (!empty($user)) ? [$entity->id, $modelName, $user['username']] : [$entity->id, $modelName];
        
        // Fields changed
        $message .= "\nChanges in fields:";
        foreach ($updated as $field => $content) {
            $message .= "\n  Field \"{".$fieldCounter++."}\" changed from \"{".$fieldCounter++."}\" to \"{".$fieldCounter++."}\"";
            $variables[] = __d('Watchdog', Inflector::humanize($field));
            $variables[] = $original[$field];
            $variables[] = $content;
        }
        
        // Changes in translations 
        if (!empty($translations)) {
            $message .= "\n\nChanges in translations:";
            foreach ($translations as $lang => $translatedContent) {
                $message .= "\n Fields changed in \"{".$fieldCounter++."}\" language:";
                $variables[] = $lang;
                foreach ($translatedContent['updated'] as $field => $content) {
                    $message .= "\n  Field \"{".$fieldCounter++."}\" changed from \"{".$fieldCounter++."}\" to \"{".$fieldCounter++."}\"";
                    $variables[] = __d('Watchdog', Inflector::humanize($field));
                    $variables[] = $translatedContent['original'][$field];
                    $variables[] = $content;
                }
            }
        }
        
        // Changes in associated models TO-DO
        
        
        // Save Log into Watchdog
        $dataLog = [
            'uid' => (!empty($user)) ? $user['id'] : 0,
            'type' => $modelName,
            'foreign_key' => $entity->id,
            'message' => $message,
            'variables' => json_encode($variables),
            'severity' => 6,
            'link' => Router::url(Router::getRequest()->here, true),
            'location' => Router::url(Router::getRequest()->url, true),
            'referer' => Router::getRequest()->env('HTTP_REFERER'),
            'hostname' => Router::getRequest()->clientIp(),
            'timestamp' => time(),
        ];
        $Watchdogs = TableRegistry::get('Watchdogs');
        $watchdog = $Watchdogs->newEntity();
        $watchdog = $Watchdogs->patchEntity($watchdog, $dataLog);
        return $Watchdogs->save($watchdog);
    }
    
    /**
     * .
     *
     * @param Cake\Event\Event The Model event that is enclosed inside a transaction
     * @param Cake\Datasource\EntityInterface $entity The entity that is to be saved or deleted
     * @param ArrayObject $options Options array containing the `_auditQueue` key
     * @return void
     */
    public function afterDelete(Event $event, EntityInterface $entity, $options)
    {
        $config = $this->_config;
        
        if (empty($config['whitelist'])) {
            $config['whitelist'] = $this->_table->schema()->columns();
        }
        
        $config['whitelist'] = array_diff($config['whitelist'], $config['blacklist']);
        
        $fields = $entity->extract($config['whitelist']);
        
        // Get main model alias
        $modelName = __d('Watchdog', Inflector::singularize(Inflector::humanize($entity->source())));
        // Get User
        $user = Router::getRequest()->session()->read('Auth.User');
        
        $fieldCounter = 0;
        $message = (!empty($user)) ? "The registry with id \"{".$fieldCounter++."}\" of table \"{".$fieldCounter++."}\" has been deleted by {".$fieldCounter++."}." : "The registry with id {".$fieldCounter++."} of table \"{".$fieldCounter++."}\" has been deleted.";
        $variables = (!empty($user)) ? [$entity->id, $modelName, $user['username']] : [$entity->id, $modelName];
        
        // Fields
        $message .= "\nFields:";
        foreach ($fields as $field => $content) {
            $message .= "\n  Field \"{".$fieldCounter++."}\": \"{".$fieldCounter++."}\"";
            $variables[] = __d('Watchdog', Inflector::humanize($field));
            $variables[] = $content;
        }
        
        // Save Log into Watchdog
        $dataLog = [
            'uid' => (!empty($user)) ? $user['id'] : 0,
            'type' => $modelName,
            'foreign_key' => $entity->id,
            'message' => $message,
            'variables' => json_encode($variables),
            'severity' => 6,
            'link' => Router::url(Router::getRequest()->here, true),
            'location' => Router::url(Router::getRequest()->url, true),
            'referer' => Router::getRequest()->env('HTTP_REFERER'),
            'hostname' => Router::getRequest()->clientIp(),
            'timestamp' => time(),
        ];
        
        $Watchdogs = TableRegistry::get('Watchdogs');
        $watchdog = $Watchdogs->newEntity();
        $watchdog = $Watchdogs->patchEntity($watchdog, $dataLog);
        return $Watchdogs->save($watchdog);
    }
    
    /**
     * Helper method used to get the property names of associations for a table.
     *
     * @param array $associated Whitelist of associations to look for
     * @return array List of property names
     */
    protected function getAssociationProperties($associated)
    {
        $associations = $this->_table->associations();
        $result = [];
        foreach ($associated as $name) {
            $result[] = $associations->get($name)->property();
        }
        
        return $result;
    }
    
    /**
     * Helper method used to get the translated fields.
     *
     * @param array $associated Whitelist of associations to look for
     * @return array List of property names
     */
    protected function getTranslatedFields($entity)
    {
        $translations = (array)$entity->get('_translations');
        
        if (empty($translations) && !$entity->dirty('_translations')) {
            return;
        }
        
        $fields = [];
        foreach ($translations as $lang => $translatedEntity) {
            if ($translatedEntity->dirty() && !empty($translatedEntity->extract(array_merge($translatedEntity->visibleProperties(), $translatedEntity->hiddenProperties())))) {
                $fields[$lang]['updated'] = $translatedEntity->extract(array_merge($translatedEntity->visibleProperties(), $translatedEntity->hiddenProperties()));
                $fields[$lang]['original'] = $translatedEntity->extractOriginal(array_keys($fields[$lang]['updated']));
                
                // Clean not changed fields
                $updatedAux = array_diff($fields[$lang]['updated'], $fields[$lang]['original']);
                if (!empty($updatedAux)) {
                    $fields[$lang]['original'] = array_diff($fields[$lang]['original'], $fields[$lang]['updated']);
                    $fields[$lang]['updated'] = $updatedAux;
                }else{
                    unset($fields[$lang]);
                }
            }
        }
        
        return $fields;
    }
}
