<?php
/**
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
use Cake\Core\Configure;
use Cake\Routing\Router;

Router::plugin('Masanchez/Watchdog', ['path' => '/watchdogs'], function ($routes) {
    $routes->fallbacks('DashedRoute');
});
Router::connect('/watchdogs', ['plugin' => 'Masanchez/Watchdog', 'controller' => 'Watchdogs', 'action' => 'index']);