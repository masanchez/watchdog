<?php
use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Initial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('watchdogs');
        $table
            ->addColumn('uid', 'uuid', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('type', 'string', [
                'default' => null,
                'limit' => 64,
                'null' => false,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('message', 'text', [
                'default' => null,
                'limit' => MysqlAdapter::TEXT_LONG,
                'null' => false,
            ])
            ->addColumn('variables', 'text', [
                'default' => null,
                'limit' => MysqlAdapter::TEXT_LONG,
                'null' => false,
            ])
            ->addColumn('severity', 'integer', [
                'default' => null,
                'limit' => MysqlAdapter::INT_TINY,
                'null' => false,
            ])
            ->addColumn('link', 'text', [
                'default' => null,
                'limit' => MysqlAdapter::TEXT_REGULAR,
                'null' => true,
            ])
            ->addColumn('location', 'text', [
                'default' => null,
                'limit' => MysqlAdapter::TEXT_REGULAR,
                'null' => false,
            ])
            ->addColumn('referer', 'text', [
                'default' => null,
                'limit' => MysqlAdapter::TEXT_REGULAR,
                'null' => true,
            ])
            ->addColumn('hostname', 'string', [
                'default' => null,
                'limit' => 128,
                'null' => false,
            ])
            ->addColumn('timestamp', 'integer', [
                'default' => true,
                'null' => false,
            ])
            ->create();
    }
}
