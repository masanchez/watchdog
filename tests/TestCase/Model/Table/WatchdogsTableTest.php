<?php
namespace Masanchez\Watchdog\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Masanchez\Watchdog\Model\Table\WatchdogsTable;

/**
 * Masanchez\Watchdog\Model\Table\WatchdogsTable Test Case
 */
class WatchdogsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.masanchez/watchdog.watchdogs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Watchdogs') ? [] : ['className' => 'Masanchez\Watchdog\Model\Table\WatchdogsTable'];
        $this->Watchdogs = TableRegistry::get('Watchdogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Watchdogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
