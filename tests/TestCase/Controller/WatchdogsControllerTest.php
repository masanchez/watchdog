<?php
namespace Masanchez\Watchdog\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Masanchez\Watchdog\Controller\WatchdogsController;

/**
 * Masanchez\Watchdog\Controller\WatchdogsController Test Case
 */
class WatchdogsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.masanchez/watchdog.watchdogs'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
